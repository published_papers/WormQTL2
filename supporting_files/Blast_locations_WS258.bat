cd G:\PhD Nema\Projects\WormQTL_III\eQTL_studies\WB258

REM Download current release of WormBase
REM powershell -Command "(New-Object Net.WebClient).DownloadFile('ftp://ftp.wormbase.org/pub/wormbase/releases/WS258/species/c_elegans/PRJNA13758/c_elegans.PRJNA13758.WS258.genomic.fa.gz', 'c_elegans.PRJNA13758.WS258.genomic.fa.gz')"
REM powershell -Command "(New-Object Net.WebClient).DownloadFile('ftp://ftp.wormbase.org/pub/wormbase/releases/WS258/species/c_elegans/PRJNA13758/c_elegans.PRJNA13758.WS258.canonical_geneset.gtf.gz', 'c_elegans.PRJNA13758.WS258.canonical_geneset.gtf.gz')"

REM unzip
powershell -Command "& { Add-Type -A 'System.IO.Compression.FileSystem'; [IO.Compression.ZipFile]::ExtractToDirectory('c_elegans.PRJNA13758.WS258.genomic.fa.gz', '.'); }"
