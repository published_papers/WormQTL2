Mark Sterken, 2019/04/11

- RILs from Noble, 2015
- QG5: him-5 in AB2; QG1199: him-5 in CB4856
- CB4856 decoded as -1, AB2 decoded as 1
- WS version in paper was unclear, took the SNP locations from Andersen, 2015 (based on WS244)
