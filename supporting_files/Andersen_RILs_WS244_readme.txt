Mark Sterken, 2019/03/02

- RILs from Andersen
- Processed by Mark Sterken; downloaded from G3 paper: A Powerful New Quantitative Genetics Platform, Combining Caenorhabditis elegans High-Throughput Fitness Assays with a Large Collection of Recombinant Strains
- Citation: Andersen, 2015
- WS244
