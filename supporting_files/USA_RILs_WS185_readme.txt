Mark Sterken, 2015-03-10

- RILs from Rockman, Kruglyak
- Processed by Basten Snoek & Mark Sterken
- Citation: Rockman, 2009; Snoek 2013 (wormQTL)
- WS185
