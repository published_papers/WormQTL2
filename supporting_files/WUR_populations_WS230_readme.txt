Mark Sterken, 2015-03-10

- integrated map of sequencing data & snp primer data
- genotypes between snp markers was linearly interpolated
- several corrections made
- citation: Thompson, 2015
- ws230


#############################Corrections ############################################################

genotype	correction_left	correction_right	chance_to	comment
CB4856	NA	NA	NA	NA
ewir001	NA	NA	NA	extra introgression on CHRII (13.4-13.74Mb) and CHRIV (0.69-1.49Mb)
ewir002	NA	NA	NA	NA
ewir005	NA	NA	NA	error in original genotyping
ewir007	NA	NA	NA	NA
ewir009	NA	NA	NA	NA
ewir011	NA	NA	NA	extra introgression on CHRII (13.2-13.37Mb) and CHRV (16.55-End Mb)
ewir012	NA	NA	NA	NA
ewir016	NA	NA	NA	NA
ewir017	NA	NA	NA	NA
ewir019	NA	NA	NA	extra introgression on CHRV (18.77-19Mb)
ewir020	NA	NA	NA	extra introgression on CHRV (18.77-19.01Mb), corrected chrII introgression: 0-2.91Mb
ewir021	NA	NA	NA	marker egPF202 is not CB (also confirmed by other experiments, Joost Riksen)
ewir023	NA	NA	NA	NA
ewir025	NA	NA	NA	NA
ewir026	NA	NA	NA	NA
ewir027	NA	NA	NA	NA
ewir028	NA	NA	NA	marker egPT209 is not CB
ewir029	NA	NA	NA	NA
ewir030	NA	NA	NA	Larger than original genotype
ewir032	NA	NA	NA	NA
ewir034	NA	NA	NA	extra introgression on CHRIII (11.59-12.15Mb)
ewir035	NA	NA	NA	NA
ewir039	NA	NA	NA	NA
ewir041	NA	NA	NA	NA
ewir042	NA	NA	NA	smaller than original genotype
ewir043	NA	NA	NA	Larger than original genotype
ewir044	NA	NA	NA	NA
ewir045	NA	NA	NA	NA
ewir046	NA	NA	NA	NA
ewir048	NA	NA	NA	NA
ewir050	NA	NA	NA	NA
ewir051	NA	NA	NA	NA
ewir052	NA	NA	NA	NA
ewir054	NA	NA	NA	NA
ewir055	NA	NA	NA	NA
ewir058	NA	NA	NA	NA
ewir060	NA	NA	NA	NA
ewir066	NA	NA	NA	NA
ewir067	NA	NA	NA	NA
ewir068	NA	NA	NA	NA
ewir070	NA	NA	NA	NA
ewir071	NA	NA	NA	NA
ewir073	NA	NA	NA	NA
ewir074	NA	NA	NA	extra introgression on CHRII (13.09-14.18Mb)
ewir076	NA	NA	NA	smaller than original genotype, marker pkP5075 is not CB
ewir077	NA	NA	NA	NA
ewir078	NA	NA	NA	NA
ewir079	NA	NA	NA	NA
ewir080	NA	NA	NA	NA
ewir081	NA	NA	NA	NA
ewir086	NA	NA	NA	NA
ewir087	NA	NA	NA	NA
ewir088	NA	NA	NA	NA
ewir089	NA	NA	NA	NA
ewir090	NA	NA	NA	NA
N2_original	NA	NA	NA	No SNPs detected
N2_repeat	NA	NA	NA	No SNPs detected
wn013	NA	NA	NA	NA
wn020	NA	NA	NA	NA
wn021	NA	NA	NA	NA
wn031	IV_10890000	IV_11190000	CB	"Extra introgression of N2 on CHRIV (10.91-11.19Mb), Extra introgression of CB on CHRV (19.18-EndMb)"
wn034	NA	NA	NA	NA
wn038	NA	NA	NA	NA
wn041	NA	NA	NA	Marker pkP6120 is not N2
wn046	NA	NA	NA	NA
wn048	NA	NA	NA	NA
wn049	NA	NA	NA	NA
wn054	NA	NA	NA	NA
wn057	NA	NA	NA	NA
wn058	NA	NA	NA	NA
wn067	NA	NA	NA	NA
wn071	NA	NA	NA	Extra introgression of CB on CHRIV (2.51-2.81Mb)
wn072	X_0	X_1020000	N2	Extra introgression of CB on CHRX (1.26-3.04Mb)
wn076_original	NA	NA	NA	"Only 20000bp off on CHRI (13.89 vs 13.87Mb), rest exactly the same"
wn076_repeat	NA	NA	NA	"Only 20000bp off on CHRI (13.87 vs 13.89Mb), rest exactly the same"
wn098	NA	NA	NA	NA
wn105	I_2510000	I_3030000	CB	Extra introgression of N2 on CHRI (2.53-3.03Mb)
wn107	NA	NA	NA	NA
wn109	NA	NA	NA	"Extra introgression of N2 on CHRI (1.6-2.62Mb), zeel-1/peel-1 locus"
wn110	NA	NA	NA	NA
wn113	NA	NA	NA	NA
wn116	NA	NA	NA	Marker egPA410 is not CB
wn124	NA	NA	NA	NA
wn128	NA	NA	NA	"Extra introgression of N2 on CHRI (2.19-2.49Mb), zeel-1/peel-1 locus, marker pkP5076 is not N2"
wn129	NA	NA	NA	NA
wn134	NA	NA	NA	NA
wn135	NA	NA	NA	"Extra introgression of N2 on CHRI (2.17-2.77Mb), zeel-1/peel-1 locus"
wn140	NA	NA	NA	NA
wn142	NA	NA	NA	marker pkP5076 is not N2
wn146	NA	NA	NA	marker pkP5076 is not N2
wn152	NA	NA	NA	"Extra introgression of N2 on CHRI (1.57-3.1Mb), zeel-1/peel-1 locus, marker pkP4045 is not N2"
wn153	NA	NA	NA	NA
wn158	NA	NA	NA	NA
wn161	IV_1390000	IV_1840000	CB	Extra introgression of N2 on CHRIV (1.41-1.84Mb)
wn162	NA	NA	NA	NA
wn171	NA	NA	NA	marker pkP5076 is not N2
wn174	NA	NA	NA	marker pkP5076 is not N2
wn176	NA	NA	NA	"Extra introgression of N2 on CHRI (2.1-3.31Mb), zeel-1/peel-1 locus"
wn177	NA	NA	NA	NA
wn185	NA	NA	NA	Extra introgression of CB on CHRIV (2.23-2.46Mb)
wn186	IV_1990000	IV_2090000	N2	Extra introgression of CB on CHRIV (2.01-2.09Mb)
wn190	NA	NA	NA	NA
wn195	IV_1650000	IV_1940000	CB	NA
wn291	NA	NA	NA	Not genotyped before
wn292	NA	NA	NA	Not genotyped before
wn293_original	NA	NA	NA	"Not genotyped before, on CHRI 10000bp difference between border (1.43 vs 1.44), on CHRII 10000bp difference between border (11.83 vs 11.84)"
wn293_repeat	NA	NA	NA	"Not genotyped before, on CHRI 10000bp difference between border (1.44 vs 1.43), on CHRII 10000bp difference between border (11.84 vs 11.83)"
wn294	I_9100000	I_12730000	N2	Not genotyped before