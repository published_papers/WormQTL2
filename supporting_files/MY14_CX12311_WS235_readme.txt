Mark Sterken, 2019-03-13

- RILs from Greene et al, 2016
- Processed by Mark Sterken & Margi Hartanto
- Citation: Greene et al, 2016
- WS235; My14 coded as -1, CX12311 as 1. Heterozygous as 0.