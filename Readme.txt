Li_2006
- dataset downloaded from GEO: GSE5395 (Li et al, 2006; Plos Genetics)
- genetic map from Li et al, 2006; Plos Genetics & Thompson et al, 2015; Genetics
- Washington state university arrays (GPL4038)
- WLS performed, corrections made


Li_2010
- dataset downloaded from GEO: GSE15778 (Li et al, 2010; Genetics)
- genetic map from Li et al, 2006; Plos Genetics & Thompson et al, 2015; Genetics
- Affimetrix 1.0 C. elegans tiling array (GPL5634); array downloadable as WS170 or WS140; normalized data from study unclear versioning
- raw data was re-normalized
- WLS performed, corrections made (also batch corrections, as in the original)


Vinuela_Snoek_2010
- dataset downloaded from GEO: GSE17071 (Vinuela & Snoek et al, 2010; Genome Research)
- genetic map from Li et al, 2006; Plos Genetics & Thompson et al, 2015; Genetics
- Washington state university arrays (GPL4038)
- samples checked before publication (Vinuela & Snoek et al, 2010; Genome Research)


Rockman_2010
- dataset downloaded from GEO: GSE23857 (Rockman et al, 2010; science)
- genetic map from Rockman et al, 2009; Plos Genetics
- Agilent arrays (GPL7727)
- Samples checked and corrected for switches earlier (Snoek et al., 2013; NAR)


Snoek_Sterken_2017
- dataset downloaded from ArrayExpress: E-MTAB-5779 (Snoek & Sterken et al, 2017; BMC Genomics)
- genetic map from Li et al, 2006; Plos Genetics & Thompson et al, 2015; Genetics
- Agilent arrays (A-MEXP-2316)
- samples checked before publication (Snoek & Sterken et al, 2017; BMC Genomics)


Sterken_2017
- dataset downloaded form ArrayExpress: E-MTAB-5856 (Sterken et al, 2017; G3)
- genetic map from Schmid et al, 2015 (PLoS Genetics)
- Agilent arrays (A-MEXP-2316)
- Samples and genotypes checked before publication (Sterken et al, 2017; G3)




