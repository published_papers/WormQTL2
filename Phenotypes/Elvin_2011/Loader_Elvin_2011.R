################################################################################
###QTL mapping

       tmp <- dplyr::filter(data_nu,censor == "no") %>%
              dplyr::mutate(trait_comp=paste(Connective,Connective_trait,treatment,trait,statistic,unit,sep=";")) %>%
              dplyr::select(strain,trait_comp,value) %>%
              tidyr::spread(key=strain,value=value)

        Elvin_2011.QTL <- data.matrix(tmp[,-1])
            rownames(Elvin_2011.QTL) <- tmp[,1]
            
        Elvin_2011.QTL <- QTL.data.prep(Elvin_2011.QTL,strain.trait=colnames(Elvin_2011.QTL),strain.map=popmap,strain.marker=popmrk)
                                     lapply(Elvin_2011.QTL,head)
        

        Elvin_2011.QTL <- map.1(Elvin_2011.QTL[[1]],Elvin_2011.QTL[[2]],Elvin_2011.QTL[[3]])
        save(Elvin_2011.QTL,file=paste(data_dir,"/QTL/obj_Elvin_2011.QTL.Rdata",sep=""))
    
        ###FDR
        for(i in 1:100){
            Elvin_2011.QTL.perm <- map.1.perm(Elvin_2011.QTL[[3]],Elvin_2011.QTL[[4]],Elvin_2011.QTL[[5]],1)
            save(Elvin_2011.QTL.perm,file=paste(data_dir,"/QTL/FDR/obj_Elvin_2011.QTL.perm",i,".Rdata",sep=""))
        }
    
        filenames.perm <- dir(paste(data_dir,"/QTL/FDR",sep=""))
        Elvin_2011.FDR <- map.perm.fdr(map1.output = Elvin_2011.QTL,filenames.perm = filenames.perm,FDR_dir = paste(data_dir,"QTL/FDR/",sep=""),q.value = 0.05)
        Elvin_2011.FDR[[1]]
        save(Elvin_2011.FDR,file=paste(data_dir,"/QTL/obj_Elvin_2011.FDR.Rdata",sep=""))
        
        ###Heritability
        Elvin_2011_h2 <- h2.narrow(Elvin_2011.QTL,n.perm=100)
        
        save(Elvin_2011_h2,file=paste(data_dir,"/QTL/obj_Elvin_2011_h2.Rdata",sep=""))

                
        ###Call peaks
        Elvin_2011.peak.QTL <- mapping.to.list(map1.output=Elvin_2011.QTL) %>%
                               peak.finder(threshold=1.3)
        
        save(Elvin_2011.peak.QTL,file=paste(data_dir,"/QTL/obj_Elvin_2011.peak.QTL.Rdata",sep=""))
        
        Elvin_2011.QTL.table <- filter(Elvin_2011.peak.QTL,!is.na(qtl_peak)) %>%
                                eQTL.table.addR2(Elvin_2011.QTL) %>%
                                merge(Elvin_2011_h2,by.x=1,by.y=1) %>%
                                mutate(qtl_FDR0.05=Elvin_2011.FDR[[1]]/10) %>%
                                separate(trait,into=c("Connective","Connective_trait","treatment","trait","statistic","unit"),sep=";")
            
        
        save(Elvin_2011.QTL.table,file=paste(data_dir,"/QTL/obj_Elvin_2011.QTL.table.Rdata",sep=""))
         
################################################################################
###Plots!

     ###Recovery  
            myColors <- c("black",brewer.pal(8,"Set2")[c(7,8)])
            names(myColors) <- c("cis","trans","none")
            plotcols <- scale_colour_manual(name = "a",values = myColors)
            plotfill <- scale_fill_manual(name = "a",values = myColors)
            
        data.plot <- mutate(Elvin_2011.QTL.table,gene_chromosome="I",gene_bp=1000,qtl_type="trans") %>%
                     prep.ggplot.eQTL.table() %>%
                     mutate(trait=ifelse(treatment=="",unique(trait)[1],trait),
                            qtl_significance=ifelse(treatment=="",NA,qtl_significance),
                            treatment=ifelse(treatment=="",unique(treatment)[1],treatment))
        
        plot.nu <- ggplot(data.plot,aes(x=qtl_bp,y=trait,colour=qtl_significance)) +
                   geom_point() + geom_segment(aes(x=qtl_bp_left,xend=qtl_bp_right,y=trait,yend=trait)) +
                   facet_grid(treatment~qtl_chromosome,scales="free",space="free") + scale_colour_gradientn(colours=brewer.pal(11,"RdYlBu"),na.value=NA,name="Significance\n(-log10(P))") +
                   presentation + scale_x_continuous(breaks=c(5,10,15,20)*10^6,labels=c(5,10,15,20)) + xlab("QTL location (Mbp)") + ylab("Trait Elvin_2011")

        pdf(file=paste(data_dir,"/QTL/Elvin_2011.pdf",sep=""),width=8,height=10)
            plot.nu
        dev.off()
        
       
    
    
    