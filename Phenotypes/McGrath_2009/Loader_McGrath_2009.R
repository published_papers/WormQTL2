################################################################################
###QTL mapping

       tmp <- dplyr::filter(data_nu,censor == "no") %>%
              dplyr::mutate(trait_comp=paste(Connective,Connective_trait,treatment,trait,statistic,unit,sep=";")) %>%
              dplyr::select(strain,trait_comp,value) %>%
              tidyr::spread(key=strain,value=value)
    
        
        McGrath_2009.QTL <- data.matrix(tmp[,-1])
            rownames(McGrath_2009.QTL) <- tmp[,1]
            
        McGrath_2009.QTL <- QTL.data.prep(McGrath_2009.QTL,strain.trait=colnames(McGrath_2009.QTL),strain.map=popmap,strain.marker=popmrk)
                                     lapply(McGrath_2009.QTL,head)
        

        McGrath_2009.QTL <- map.1(McGrath_2009.QTL[[1]],McGrath_2009.QTL[[2]],McGrath_2009.QTL[[3]])
        save(McGrath_2009.QTL,file=paste(data_dir,"/QTL/obj_McGrath_2009.QTL.Rdata",sep=""))
    
        ###FDR
        for(i in 1:100){
            McGrath_2009.QTL.perm <- map.1.perm(McGrath_2009.QTL[[3]],McGrath_2009.QTL[[4]],McGrath_2009.QTL[[5]],1)
            save(McGrath_2009.QTL.perm,file=paste(data_dir,"/QTL/FDR/obj_McGrath_2009.QTL.perm",i,".Rdata",sep=""))
        }
    
        filenames.perm <- dir(paste(data_dir,"/QTL/FDR",sep=""))
        McGrath_2009.FDR <- map.perm.fdr(map1.output = McGrath_2009.QTL,filenames.perm = filenames.perm,FDR_dir = paste(data_dir,"QTL/FDR/",sep=""),q.value = 0.05)
        McGrath_2009.FDR[[1]]
        save(McGrath_2009.FDR,file=paste(data_dir,"/QTL/obj_McGrath_2009.FDR.Rdata",sep=""))
        
        ###Heritability
        McGrath_2009_h2 <- h2.narrow(McGrath_2009.QTL,n.perm=100)
        
        save(McGrath_2009_h2,file=paste(data_dir,"/QTL/obj_McGrath_2009_h2.Rdata",sep=""))

        
        ###Call peaks
        McGrath_2009.peak.QTL <- mapping.to.list(map1.output=McGrath_2009.QTL) %>%
                                 peak.finder(threshold=3.0)
        
        save(McGrath_2009.peak.QTL,file=paste(data_dir,"/QTL/obj_McGrath_2009.peak.QTL.Rdata",sep=""))
        
        McGrath_2009.QTL.table <- filter(McGrath_2009.peak.QTL,!is.na(qtl_peak)) %>%
                                  eQTL.table.addR2(McGrath_2009.QTL) %>%
                                  merge(McGrath_2009_h2,by.x=1,by.y=1) %>%
                                  mutate(qtl_FDR0.05=McGrath_2009.FDR[[1]]/10) %>%
                                  separate(trait,into=c("Connective","Connective_trait","treatment","trait","statistic","unit"),sep=";")
            
        
        save(McGrath_2009.QTL.table,file=paste(data_dir,"/QTL/obj_McGrath_2009.QTL.table.Rdata",sep=""))
         
################################################################################
###Plots!

     ###Recovery  
            myColors <- c("black",brewer.pal(8,"Set2")[c(7,8)])
            names(myColors) <- c("cis","trans","none")
            plotcols <- scale_colour_manual(name = "a",values = myColors)
            plotfill <- scale_fill_manual(name = "a",values = myColors)
            
        data.plot <- mutate(McGrath_2009.QTL.table,gene_chromosome="I",gene_bp=1000,qtl_type="trans") %>%
                     prep.ggplot.eQTL.table() %>%
                     mutate(trait=ifelse(treatment=="",unique(trait)[1],trait),
                            qtl_significance=ifelse(treatment=="",NA,qtl_significance),
                            treatment=ifelse(treatment=="",unique(treatment)[1],treatment))
        
        plot.nu <- ggplot(data.plot,aes(x=qtl_bp,y=trait,colour=qtl_significance)) +
                   geom_point() + geom_segment(aes(x=qtl_bp_left,xend=qtl_bp_right,y=trait,yend=trait)) +
                   facet_grid(treatment~qtl_chromosome,scales="free",space="free") + scale_colour_gradientn(colours=brewer.pal(11,"RdYlBu"),na.value=NA,name="Significance\n(-log10(P))") +
                   presentation + scale_x_continuous(breaks=c(5,10,15,20)*10^6,labels=c(5,10,15,20)) + xlab("QTL location (Mbp)") + ylab("Trait McGrath_2009")

        pdf(file=paste(data_dir,"/QTL/McGrath_2009.pdf",sep=""),width=8,height=10)
            plot.nu
        dev.off()
        
       
    
    
    