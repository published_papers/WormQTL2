################################################################################
###QTL mapping

       tmp <- dplyr::filter(data_nu,censor == "no") %>%
              dplyr::mutate(trait_comp=paste(Connective,Connective_trait,treatment,trait,statistic,unit,sep=";")) %>%
              dplyr::select(strain,trait_comp,value) %>%
              tidyr::spread(key=strain,value=value)
    
        
        Stastna_2015.QTL <- data.matrix(tmp[,-1])
            rownames(Stastna_2015.QTL) <- tmp[,1]
            
        Stastna_2015.QTL <- QTL.data.prep(Stastna_2015.QTL,strain.trait=colnames(Stastna_2015.QTL),strain.map=popmap,strain.marker=popmrk)
                                     lapply(Stastna_2015.QTL,head)
        

        Stastna_2015.QTL <- map.1(Stastna_2015.QTL[[1]],Stastna_2015.QTL[[2]],Stastna_2015.QTL[[3]])
        save(Stastna_2015.QTL,file=paste(data_dir,"/QTL/obj_Stastna_2015.QTL.Rdata",sep=""))
    
        ###FDR
        for(i in 1:100){
            Stastna_2015.QTL.perm <- map.1.perm(Stastna_2015.QTL[[3]],Stastna_2015.QTL[[4]],Stastna_2015.QTL[[5]],1)
            save(Stastna_2015.QTL.perm,file=paste(data_dir,"/QTL/FDR/obj_Stastna_2015.QTL.perm",i,".Rdata",sep=""))
        }
    
        filenames.perm <- dir(paste(data_dir,"/QTL/FDR",sep=""))
        Stastna_2015.FDR <- map.perm.fdr(map1.output = Stastna_2015.QTL,filenames.perm = filenames.perm,FDR_dir = paste(data_dir,"QTL/FDR/",sep=""),q.value = 0.05)
        Stastna_2015.FDR[[1]]
        save(Stastna_2015.FDR,file=paste(data_dir,"/QTL/obj_Stastna_2015.FDR.Rdata",sep=""))
        
        ###Heritability
        Stastna_2015_h2 <- h2.narrow(Stastna_2015.QTL,n.perm=100)
        
        save(Stastna_2015_h2,file=paste(data_dir,"/QTL/obj_Stastna_2015_h2.Rdata",sep=""))

        
        ###Call peaks
        Stastna_2015.peak.QTL <- mapping.to.list(map1.output=Stastna_2015.QTL) %>%
                               peak.finder(threshold=1.3)
        
        save(Stastna_2015.peak.QTL,file=paste(data_dir,"/QTL/obj_Stastna_2015.peak.QTL.Rdata",sep=""))
        
        Stastna_2015.QTL.table <- filter(Stastna_2015.peak.QTL,!is.na(qtl_peak)) %>%
                              eQTL.table.addR2(Stastna_2015.QTL) %>%
                              merge(Stastna_2015_h2,by.x=1,by.y=1) %>%
                              mutate(qtl_FDR0.05=Stastna_2015.FDR[[1]]/10) %>%
                              separate(trait,into=c("Connective","Connective_trait","treatment","trait","statistic","unit"),sep=";")
            
        
        save(Stastna_2015.QTL.table,file=paste(data_dir,"/QTL/obj_Stastna_2015.QTL.table.Rdata",sep=""))
         
################################################################################
###Plots!

     ###Recovery  
            myColors <- c("black",brewer.pal(8,"Set2")[c(7,8)])
            names(myColors) <- c("cis","trans","none")
            plotcols <- scale_colour_manual(name = "a",values = myColors)
            plotfill <- scale_fill_manual(name = "a",values = myColors)
            
        data.plot <- mutate(Stastna_2015.QTL.table,gene_chromosome="I",gene_bp=1000,qtl_type="trans") %>%
                     prep.ggplot.eQTL.table() %>%
                     mutate(trait=ifelse(treatment=="",unique(trait)[1],trait),
                            qtl_significance=ifelse(treatment=="",NA,qtl_significance),
                            treatment=ifelse(treatment=="",unique(treatment)[1],treatment))
        
        plot.nu <- ggplot(data.plot,aes(x=qtl_bp,y=trait,colour=qtl_significance)) +
                   geom_point() + geom_segment(aes(x=qtl_bp_left,xend=qtl_bp_right,y=trait,yend=trait)) +
                   facet_grid(treatment~qtl_chromosome,scales="free",space="free") + scale_colour_gradientn(colours=brewer.pal(11,"RdYlBu"),na.value=NA,name="Significance\n(-log10(P))") +
                   presentation + scale_x_continuous(breaks=c(5,10,15,20)*10^6,labels=c(5,10,15,20)) + xlab("QTL location (Mbp)") + ylab("Trait Stastna_2015")

        pdf(file=paste(data_dir,"/QTL/Stastna_2015.pdf",sep=""),width=8,height=10)
            plot.nu
        dev.off()
        
       
    
    
    