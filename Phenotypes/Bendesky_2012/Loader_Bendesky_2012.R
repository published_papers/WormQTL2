################################################################################
###QTL mapping

       tmp <- dplyr::filter(data_nu,censor == "no",!strain %in% c("QX126","QX175")) %>%
              dplyr::mutate(trait_comp=paste(Connective,Connective_trait,treatment,trait,statistic,unit,sep=";")) %>%
              dplyr::select(strain,trait_comp,value) %>%
              tidyr::spread(key=strain,value=value)
    
        
        Bendesky_2012.QTL <- data.matrix(tmp[,-1])
            rownames(Bendesky_2012.QTL) <- tmp[,1]
            
        Bendesky_2012.QTL <- QTL.data.prep(Bendesky_2012.QTL,strain.trait=colnames(Bendesky_2012.QTL),strain.map=popmap,strain.marker=popmrk)
                                     lapply(Bendesky_2012.QTL,head)
        

        Bendesky_2012.QTL <- map.1(Bendesky_2012.QTL[[1]],Bendesky_2012.QTL[[2]],Bendesky_2012.QTL[[3]])
        save(Bendesky_2012.QTL,file=paste(data_dir,"/QTL/obj_Bendesky_2012.QTL.Rdata",sep=""))
    
        ###FDR
        for(i in 1:100){
            Bendesky_2012.QTL.perm <- map.1.perm(Bendesky_2012.QTL[[3]],Bendesky_2012.QTL[[4]],Bendesky_2012.QTL[[5]],1)
            save(Bendesky_2012.QTL.perm,file=paste(data_dir,"/QTL/FDR/obj_Bendesky_2012.QTL.perm",i,".Rdata",sep=""))
        }
    
        filenames.perm <- dir(paste(data_dir,"/QTL/FDR",sep=""))
        Bendesky_2012.FDR <- map.perm.fdr(map1.output = Bendesky_2012.QTL,filenames.perm = filenames.perm,FDR_dir = paste(data_dir,"QTL/FDR/",sep=""),q.value = 0.05)
        Bendesky_2012.FDR[[1]]
        save(Bendesky_2012.FDR,file=paste(data_dir,"/QTL/obj_Bendesky_2012.FDR.Rdata",sep=""))
        
        ###Heritability
        Bendesky_2012_h2 <- h2.narrow(Bendesky_2012.QTL,n.perm=100)

        save(Bendesky_2012_h2,file=paste(data_dir,"/QTL/obj_Bendesky_2012_h2.Rdata",sep=""))

        
        ###Call peaks
        Bendesky_2012.peak.QTL <- mapping.to.list(map1.output=Bendesky_2012.QTL) %>%
                                 peak.finder(threshold=1.5)
        
        save(Bendesky_2012.peak.QTL,file=paste(data_dir,"/QTL/obj_Bendesky_2012.peak.QTL.Rdata",sep=""))
        
        Bendesky_2012.QTL.table <- filter(Bendesky_2012.peak.QTL,!is.na(qtl_peak)) %>%
                                  eQTL.table.addR2(Bendesky_2012.QTL) %>%
                                  merge(Bendesky_2012_h2,by.x=1,by.y=1) %>%
                                  mutate(qtl_FDR0.05=Bendesky_2012.FDR[[1]]/10) %>%
                                  separate(trait,into=c("Connective","Connective_trait","treatment","trait","statistic","unit"),sep=";")
            
        
        save(Bendesky_2012.QTL.table,file=paste(data_dir,"/QTL/obj_Bendesky_2012.QTL.table.Rdata",sep=""))
         
################################################################################
###Plots!

     ###Recovery  
            myColors <- c("black",brewer.pal(8,"Set2")[c(7,8)])
            names(myColors) <- c("cis","trans","none")
            plotcols <- scale_colour_manual(name = "a",values = myColors)
            plotfill <- scale_fill_manual(name = "a",values = myColors)
            
        data.plot <- mutate(Bendesky_2012.QTL.table,gene_chromosome="I",gene_bp=1000,qtl_type="trans") %>%
                     prep.ggplot.eQTL.table() %>%
                     mutate(trait=ifelse(treatment=="",unique(trait)[1],trait),
                            qtl_significance=ifelse(treatment=="",NA,qtl_significance),
                            treatment=ifelse(treatment=="",unique(treatment)[1],treatment))
        
        plot.nu <- ggplot(data.plot,aes(x=qtl_bp,y=trait,colour=qtl_significance)) +
                   geom_point() + geom_segment(aes(x=qtl_bp_left,xend=qtl_bp_right,y=trait,yend=trait)) +
                   facet_grid(treatment~qtl_chromosome,scales="free",space="free") + scale_colour_gradientn(colours=brewer.pal(11,"RdYlBu"),na.value=NA,name="Significance\n(-log10(P))") +
                   presentation + scale_x_continuous(breaks=c(5,10,15,20)*10^6,labels=c(5,10,15,20)) + xlab("QTL location (Mbp)") + ylab("Trait Bendesky_2012")

        pdf(file=paste(data_dir,"/QTL/Bendesky_2012.pdf",sep=""),width=8,height=10)
            plot.nu
        dev.off()
        
       
    
    
    