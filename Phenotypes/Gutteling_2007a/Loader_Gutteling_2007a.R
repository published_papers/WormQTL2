################################################################################
###QTL mapping
    
        tmp <- filter(data_nu,censor == "no") %>%
               mutate(trait_comp=paste(Connective,Connective_trait,treatment,trait,statistic,unit,sep=";")) %>%
               select(strain,trait_comp,value) %>%
               spread(key=strain,value=value)
        
        Gutteling_2007a.QTL <- data.matrix(tmp[,-1])
            rownames(Gutteling_2007a.QTL) <- tmp[,1]
            
        Gutteling_2007a.QTL <- QTL.data.prep(Gutteling_2007a.QTL,strain.trait=colnames(Gutteling_2007a.QTL),strain.map=popmap,strain.marker=popmrk)
                                     lapply(Gutteling_2007a.QTL,head)
        

        Gutteling_2007a.QTL <- map.1(Gutteling_2007a.QTL[[1]],Gutteling_2007a.QTL[[2]],Gutteling_2007a.QTL[[3]])
        save(Gutteling_2007a.QTL,file=paste(data_dir,"/QTL/obj_Gutteling_2007a.QTL.Rdata",sep=""))
    
        ###FDR
        for(i in 1:100){
            Gutteling_2007a.QTL.perm <- map.1.perm(Gutteling_2007a.QTL[[3]],Gutteling_2007a.QTL[[4]],Gutteling_2007a.QTL[[5]],1)
            save(Gutteling_2007a.QTL.perm,file=paste(data_dir,"/QTL/FDR/obj_Gutteling_2007a.QTL.perm",i,".Rdata",sep=""))
        }
    
        filenames.perm <- dir(paste(data_dir,"/QTL/FDR",sep=""))
        Gutteling_2007a.FDR <- map.perm.fdr(map1.output = Gutteling_2007a.QTL,filenames.perm = filenames.perm,FDR_dir = paste(data_dir,"QTL/FDR/",sep=""),q.value = 0.05)
        Gutteling_2007a.FDR[[1]]
        save(Gutteling_2007a.FDR,file=paste(data_dir,"/QTL/obj_Gutteling_2007a.FDR.Rdata",sep=""))
        
        ###Heritability
        Gutteling_2007a_h2 <- h2.narrow(Gutteling_2007a.QTL)
        
        ###Call peaks
        Gutteling_2007a.peak.QTL <- mapping.to.list(map1.output=Gutteling_2007a.QTL) %>%
                                    peak.finder(threshold=1.3)
        
        save(Gutteling_2007a.peak.QTL,file=paste(data_dir,"/QTL/obj_Gutteling_2007a.peak.QTL.Rdata",sep=""))
        
        Gutteling_2007a.QTL.table <- filter(Gutteling_2007a.peak.QTL,!is.na(qtl_peak)) %>%
                                     eQTL.table.addR2(Gutteling_2007a.QTL) %>%
                                     merge(Gutteling_2007a_h2,by.x=1,by.y=1) %>%
                                     mutate(qtl_FDR0.05=Gutteling_2007a.FDR[[1]]/10) %>%
                                     separate(trait,into=c("Connective","Connective_trait","treatment","trait","statistic","unit"),sep=";")
            
        
        save(Gutteling_2007a.QTL.table,file=paste(data_dir,"/QTL/obj_Gutteling_2007a.QTL.table.Rdata",sep=""))
         
################################################################################
###Plots!

     ###Recovery  
        myColors <- c("black",brewer.pal(8,"Set2")[c(7,8)])
        names(myColors) <- c("cis","trans","none")
        plotcols <- scale_colour_manual(name = "a",values = myColors)
        plotfill <- scale_fill_manual(name = "a",values = myColors)
            
        data.plot <- mutate(Gutteling_2007a.QTL.table,gene_chromosome="I",gene_bp=1000,qtl_type="trans") %>%
                     prep.ggplot.eQTL.table() %>%
                     mutate(trait=ifelse(treatment=="",unique(trait)[1],trait),
                            qtl_significance=ifelse(treatment=="",NA,qtl_significance),
                            treatment=ifelse(treatment=="",unique(treatment)[1],treatment))
        
        plot.nu <- ggplot(data.plot,aes(x=qtl_bp,y=trait,colour=qtl_significance)) +
                   geom_point() + geom_segment(aes(x=qtl_bp_left,xend=qtl_bp_right,y=trait,yend=trait)) +
                   facet_grid(treatment~qtl_chromosome,scales="free",space="free") + scale_colour_gradientn(colours=brewer.pal(11,"RdYlBu"),na.value=NA,name="Significance\n(-log10(P))") +
                   presentation + scale_x_continuous(breaks=c(5,10,15,20)*10^6,labels=c(5,10,15,20)) + xlab("QTL location (Mbp)") + ylab("Trait Gutteling_2007a")

        pdf(file=paste(data_dir,"/QTL/Gutteling_2007a.pdf",sep=""),width=8,height=3)
            plot.nu
        dev.off()
        
       
    
    
    