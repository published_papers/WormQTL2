################################################################################
###QTL mapping

       tmp <- dplyr::filter(data_nu,censor == "no",!strain %in% c("N2","CB4856")) %>%
              dplyr::mutate(trait_comp=paste(Connective,Connective_trait,treatment,trait,statistic,unit,sep=";")) %>%
              dplyr::select(strain,trait_comp,value) %>%
              tidyr::spread(key=strain,value=value)
    
        
        Kammenga_2007.QTL <- data.matrix(tmp[,-1])
            rownames(Kammenga_2007.QTL) <- tmp[,1]
            
        Kammenga_2007.QTL <- QTL.data.prep(Kammenga_2007.QTL,strain.trait=colnames(Kammenga_2007.QTL),strain.map=popmap,strain.marker=popmrk)
                                     lapply(Kammenga_2007.QTL,head)
        

        Kammenga_2007.QTL <- map.1(Kammenga_2007.QTL[[1]],Kammenga_2007.QTL[[2]],Kammenga_2007.QTL[[3]])
        save(Kammenga_2007.QTL,file=paste(data_dir,"/QTL/obj_Kammenga_2007.QTL.Rdata",sep=""))
    
        ###FDR
        Kammenga_2007.QTL.perm <- map.1.perm(Kammenga_2007.QTL[[3]],Kammenga_2007.QTL[[4]],Kammenga_2007.QTL[[5]],1000)
        save(Kammenga_2007.QTL.perm,file=paste(data_dir,"/QTL/FDR/obj_Kammenga_2007.QTL.perm.Rdata",sep=""))

    
        filenames.perm <- dir(paste(data_dir,"/QTL/FDR",sep=""))
        Kammenga_2007.FDR <- map.perm.fdr(map1.output = Kammenga_2007.QTL,filenames.perm = filenames.perm,FDR_dir = paste(data_dir,"QTL/FDR/",sep=""),q.value = 0.05,small=TRUE)
        Kammenga_2007.FDR[[1]]
        save(Kammenga_2007.FDR,file=paste(data_dir,"/QTL/obj_Kammenga_2007.FDR.Rdata",sep=""))
        
        ###Heritability
        Kammenga_2007_h2 <- h2.narrow(Kammenga_2007.QTL,n.perm=100)

        save(Kammenga_2007_h2,file=paste(data_dir,"/QTL/obj_Kammenga_2007_h2.Rdata",sep=""))

        
        ###Call peaks
        Kammenga_2007.peak.QTL <- mapping.to.list(map1.output=Kammenga_2007.QTL) %>%
                                 peak.finder(threshold=1.5)
        
        save(Kammenga_2007.peak.QTL,file=paste(data_dir,"/QTL/obj_Kammenga_2007.peak.QTL.Rdata",sep=""))
        
        Kammenga_2007.QTL.table <- filter(Kammenga_2007.peak.QTL,!is.na(qtl_peak)) %>%
                                  eQTL.table.addR2(Kammenga_2007.QTL) %>%
                                  merge(Kammenga_2007_h2,by.x=1,by.y=1) %>%
                                  mutate(qtl_FDR0.05=Kammenga_2007.FDR[[1]]/10) %>%
                                  separate(trait,into=c("Connective","Connective_trait","treatment","trait","statistic","unit"),sep=";")
            
        
        save(Kammenga_2007.QTL.table,file=paste(data_dir,"/QTL/obj_Kammenga_2007.QTL.table.Rdata",sep=""))
         
################################################################################
###Plots!

     ###Recovery  
            myColors <- c("black",brewer.pal(8,"Set2")[c(7,8)])
            names(myColors) <- c("cis","trans","none")
            plotcols <- scale_colour_manual(name = "a",values = myColors)
            plotfill <- scale_fill_manual(name = "a",values = myColors)
            
        data.plot <- mutate(Kammenga_2007.QTL.table,gene_chromosome="I",gene_bp=1000,qtl_type="trans") %>%
                     prep.ggplot.eQTL.table() %>%
                     mutate(trait=ifelse(treatment=="",unique(trait)[1],trait),
                            qtl_significance=ifelse(treatment=="",NA,qtl_significance),
                            treatment=ifelse(treatment=="",unique(treatment)[1],treatment))
        
        plot.nu <- ggplot(data.plot,aes(x=qtl_bp,y=trait,colour=qtl_significance)) +
                   geom_point() + geom_segment(aes(x=qtl_bp_left,xend=qtl_bp_right,y=trait,yend=trait)) +
                   facet_grid(treatment~qtl_chromosome,scales="free",space="free") + scale_colour_gradientn(colours=brewer.pal(11,"RdYlBu"),na.value=NA,name="Significance\n(-log10(P))") +
                   presentation + scale_x_continuous(breaks=c(5,10,15,20)*10^6,labels=c(5,10,15,20)) + xlab("QTL location (Mbp)") + ylab("Trait Kammenga_2007")

        pdf(file=paste(data_dir,"/QTL/Kammenga_2007.pdf",sep=""),width=8,height=10)
            plot.nu
        dev.off()
        
       
    
    
    